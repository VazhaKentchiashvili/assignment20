package com.vazhasapp.assignment20.viewmodel

import android.util.Log.d
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vazhasapp.assignment20.api.ArticlesApiService
import com.vazhasapp.assignment20.api.ResponseHandler
import com.vazhasapp.assignment20.model.Article
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ArticleViewModel : ViewModel() {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    private val _articleLiveData = MutableLiveData<ResponseHandler<List<Article>>>()
    val articleLiveData: LiveData<ResponseHandler<List<Article>>> get() = _articleLiveData

    fun init() {
        getAllArticles()
    }

    private fun getAllArticles() {

        viewModelScope.launch {
            withContext(ioDispatcher) {
                val allArticles =
                    ArticlesApiService.getArticle.getAllArticle()
                if (allArticles.isSuccessful) {
                    _articleLiveData.postValue(ResponseHandler.Success(allArticles.body()))
                    _articleLiveData.postValue(ResponseHandler.Loading(false))
                } else {
                    _articleLiveData.postValue(ResponseHandler.Error(allArticles.message()))
                }
            }
        }
    }

    fun searchArticles(title: String) {
        viewModelScope.launch {
            withContext(ioDispatcher) {
                val searchedArticle =
                    ArticlesApiService.getArticle.searchArticleByTitle(title)
            }
        }
    }
}