package com.vazhasapp.assignment20.model

data class Events(
    val id: String?,
    val provider: String?
)
