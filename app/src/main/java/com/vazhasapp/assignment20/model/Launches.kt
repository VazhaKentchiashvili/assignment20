package com.vazhasapp.assignment20.model

data class Launches(
    val id: String?,
    val provider: String?
)
