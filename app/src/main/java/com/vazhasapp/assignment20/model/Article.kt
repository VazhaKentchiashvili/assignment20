package com.vazhasapp.assignment20.model

data class Article(
    val id: Int?,
    val featured: Boolean?,
    val title: String?,
    val url: String?,
    val imageUrl: String?,
    val newsSite: String?,
    val summary: String?,
    val publishedAt: String?,
    val launches: List<Launches>?,
    val events: List<Events>?,
)
