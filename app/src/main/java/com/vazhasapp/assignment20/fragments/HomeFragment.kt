package com.vazhasapp.assignment20.fragments

import android.os.Bundle
import android.util.Log.d
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.viewpager2.widget.ViewPager2
import com.vazhasapp.assignment20.adapters.ArticlesPagerAdapter
import com.vazhasapp.assignment20.api.ResponseHandler
import com.vazhasapp.assignment20.databinding.FragmentHomeBinding
import com.vazhasapp.assignment20.extensions.showIf
import com.vazhasapp.assignment20.model.Article
import com.vazhasapp.assignment20.model.Events
import com.vazhasapp.assignment20.viewmodel.ArticleViewModel

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val articleViewModel: ArticleViewModel by viewModels()

    private val viewPagerAdapter = ArticlesPagerAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        articleViewModel.init()
        setupViewPager()
        listeners()
        search()

        showCurrentArticleInfo()
    }

    private fun listeners() {
        articleViewModel.articleLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is ResponseHandler.Error -> d("Error", "${it.errorMessage}")
                is ResponseHandler.Loading -> binding.progressCircular.showIf(it.loading)
                is ResponseHandler.Success -> {
                    viewPagerAdapter.setData(it.data!!.toMutableList())
                }
            }
        })
    }

    private fun search() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    articleViewModel.searchArticles(newText)
                }
                return true
            }
        })
    }

    private fun showCurrentArticleInfo() {
        viewPagerAdapter.clickListener = { position, data ->
            val articleListObject = data!!
            binding.tvDescriptionText.text = articleListObject.summary
        }
    }

    private fun setupViewPager() {
        with(binding) {
            vpArticles.adapter = viewPagerAdapter
            vpArticles.clipToPadding = false
            vpArticles.clipChildren = false
            vpArticles.offscreenPageLimit = 3

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}