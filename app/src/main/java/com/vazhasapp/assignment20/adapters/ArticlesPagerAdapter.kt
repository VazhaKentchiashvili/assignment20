package com.vazhasapp.assignment20.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import com.vazhasapp.assignment20.api.ResponseHandler
import com.vazhasapp.assignment20.databinding.ArticlesCardViewBinding
import com.vazhasapp.assignment20.extensions.setImageFromGlide
import com.vazhasapp.assignment20.model.Article

typealias ArticleClick = (position: Int, D: Article?) -> Unit

class ArticlesPagerAdapter : RecyclerView.Adapter<ArticlesPagerAdapter.ArticlesPagerViewHolder>() {

    private val articleList = mutableListOf<Article>()
    lateinit var clickListener: ArticleClick
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ArticlesPagerViewHolder(
            ArticlesCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ArticlesPagerViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = articleList.size

    inner class ArticlesPagerViewHolder(private val binding: ArticlesCardViewBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        private lateinit var currentArticle: Article
        fun bind() {
            currentArticle = articleList[adapterPosition]

            binding.imArticleImage.setImageFromGlide(currentArticle.imageUrl.toString())
            binding.tvArticleTitle.text = currentArticle.title
            binding.tvArticleUpdateDate.text = currentArticle.publishedAt
            binding.root.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            clickListener(adapterPosition, articleList[adapterPosition])
        }
    }

    fun setData(enteredList: MutableList<Article>) {
        articleList.clear()
        articleList.addAll(enteredList)
        notifyDataSetChanged()
    }
}