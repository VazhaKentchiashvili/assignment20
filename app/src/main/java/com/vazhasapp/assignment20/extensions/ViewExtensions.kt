package com.vazhasapp.assignment20.extensions

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.vazhasapp.assignment20.R

fun ImageView.setImageFromGlide(imageUrl: String) {
    Glide.with(this.context)
        .load(imageUrl)
        .placeholder(R.drawable.ic_image_not_found)
        .into(this)
}

fun ProgressBar.showIf(status: Boolean) {
    visibility = if (status)
        View.VISIBLE
    else
        View.GONE
}
