package com.vazhasapp.assignment20.utils

object Constants {
    const val BASE_URL = "https://api.spaceflightnewsapi.net/"
    const val API_ENDPOINT = "v3/articles"
}