package com.vazhasapp.assignment20.api

import com.vazhasapp.assignment20.utils.Constants.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ArticlesApiService {

    private val retrofitBuilder: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val getArticle: ArticleApi by lazy {
        retrofitBuilder.create(ArticleApi::class.java)
    }
}