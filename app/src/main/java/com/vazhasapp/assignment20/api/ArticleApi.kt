package com.vazhasapp.assignment20.api

import com.vazhasapp.assignment20.model.Article
import com.vazhasapp.assignment20.utils.Constants.API_ENDPOINT
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticleApi {

    @GET(API_ENDPOINT)
    suspend fun getAllArticle(): Response<List<Article>>

    @GET(API_ENDPOINT)
    suspend fun searchArticleByTitle(@Query("_contains") contains: String): Response<List<Article>>

}